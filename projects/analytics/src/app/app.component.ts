import { Component } from '@angular/core';

@Component({
  selector: 'analytics-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'analytics';
}
